import React from "react";
import IconArrow from "../../assets/svg/icon-arrow-down.svg";

import "./index.css";

class CardFQA extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accordions: this.props.data,
    };
  }

  handleClick = (currentAccordion) => {
    const newAccordions = this.state.accordions;

    newAccordions.map((accordion) => {
      const showDescription = document.querySelector(
        `.description-${accordion.id}`
      );

      if (currentAccordion.id === accordion.id) {
        accordion.state = "active";
        showDescription.style.height = showDescription.scrollHeight + "px";
        showDescription.style.opacity = 1;
      } else {
        accordion.state = "inactive";
        showDescription.style.height = null;
        showDescription.style.opacity = 0;
      }
      return accordion;
    });

    this.setState({
      accordions: newAccordions,
    });
  };

  render() {
    const { accordions } = this.state;

    return (
      <>
        {accordions.map((accordion, index) => (
          <article className="card" key={index}>
            <div
              className={`card-head ${accordion.state}`}
              onClick={() => this.handleClick(accordion)}
            >
              <h3 className="card-head__title">{accordion.title}</h3>
              <img
                className="card-head__icon"
                src={IconArrow}
                alt="Icon arrow"
              />
            </div>
            <div className={`card-body description-${accordion.id}`}>
              <p className="card-body__description">{accordion.description}</p>
            </div>
          </article>
        ))}
      </>
    );
  }
}

export default CardFQA;
